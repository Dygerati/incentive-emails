import * as emails from './index';
import orderMockData from './testData/order.json';
import brandMockData from './testData/brand.json';
import fs from 'fs-extra';

console.info = jest.fn();

describe('Emails', () => {
   it('Incentive email will match snapshot', async () => {
      const compiled = await emails.compile('incentive', {
         emailId: 'testEmailId',
         cfunctions: 'http://notreally.com',
         brand: brandMockData,
         order: orderMockData,
         notificationId: 'vt2AORijdm5V48bVThlR',
         env: 'dev',
         giftCardAmount: 2
      });

      await fs.writeFile('./generated/incentive.html', compiled.content);
      expect(compiled.content).toMatchSnapshot();
   });
});
